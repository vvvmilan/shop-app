import {createContext, useEffect, useState} from 'react';

const PreferencesContext = createContext({
  isThemeDark: false,
  setIsThemeDark: () => {},
  handleThemeToggle: () => {},
});

const PreferencesProvider = ({ children }) => {
  const [isThemeDark, setIsThemeDark] = useState(() => {
    if (typeof window === 'undefined') {
      return false
    }
    try {
      const item = window.localStorage.getItem('isThemeDark')
      return item ? JSON.parse(item) : false
    } catch {
      return false
    }
  });
  // const [ isThemeDark, setIsThemeDark ] = useState(null);

  // useEffect(() => {
  //   const isThemeDarkLocalStorage = JSON.parse(localStorage.getItem('isThemeDark'));
  //   console.log('useEff Local', isThemeDarkLocalStorage)
  //   if (isThemeDarkLocalStorage === true || isThemeDarkLocalStorage === false) {
  //     setIsThemeDark(isThemeDarkLocalStorage)
  //     console.log(isThemeDark)
  //   }
  //   else localStorage.setItem('isThemeDark', isThemeDark)
  //   },[])

  useEffect(() => {
    localStorage.setItem('isThemeDark', JSON.parse(isThemeDark));
    isThemeDark
      ? document.body.classList.add('dark')
      : document.body.classList.remove('dark')
  }, [isThemeDark])

  const handleThemeToggle = () => {
    setIsThemeDark(!isThemeDark);
  };

  return (
    <PreferencesContext.Provider
      value={{ isThemeDark, setIsThemeDark, handleThemeToggle }}
    >
      {children}
    </PreferencesContext.Provider>
  );
};

export default PreferencesProvider;

export { PreferencesContext };
