import * as yup from 'yup';
import React, { useEffect, useState } from 'react';
import { ExclamationCircleIcon } from '@heroicons/react/20/solid';
import { debounce } from 'lodash';

interface SignInProps {
  signInData: object;
  setSignInData: Function;
}

// const InputEmail= (props: SignInProps) => {
const InputEmail: React.FC<SignInProps> = ({ signInData, setSignInData }) => {
  const [email, setEmail] = useState({ value: '', error: null });
  const isValid = !email.error;

  const handleInput = (e: any) => {
    const value = e.target.value;
    const validateEmail = yup.object({
      value: yup.string().email().required(),
    });

    validateEmail.validate({ value })
      .then(() => {
        setEmail({ value, error: null });
      })
      .catch((error) => {
        const errors = error.errors;
        setEmail({ value, error: errors });
      });
  };

  useEffect(() => {
    // props.setSignInData({
    setSignInData({
      ...signInData,
      // ...props.signInData,
      email,
      // email: {value: email.value, error: email.error}
    });
  }, [email]);

  return (
    <div>
      <label
        htmlFor='email'
        className='block text-sm font-medium text-gray-700 dark:text-gray-300'
      >
        Email
      </label>
      <div className='relative mt-1 rounded-md shadow-sm'>
        <input
          type='email'
          name='email'
          id='email'
          className='block w-full rounded-md border-indigo-600 pr-10 text-red-900 placeholder-red-300 focus:border-slate-500 focus:outline-none focus:ring-indigo-600 sm:text-sm'
          placeholder=''
          onInput={debounce(handleInput, 300)}
        />
        {!isValid && (
          <div className='pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3'>
            <ExclamationCircleIcon
              className='h-5 w-5 text-red-500'
              aria-hidden='true'
            />
          </div>
        )}
      </div>
      {!isValid && (
        <p className='mt-2 text-sm text-red-600' id='email-error'>
          Please enter valid email address
        </p>
      )}
    </div>
  );
};

export default InputEmail;
