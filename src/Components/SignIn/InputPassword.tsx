import * as yup from 'yup';
import React, { useEffect, useState } from 'react';
import { ExclamationCircleIcon } from '@heroicons/react/20/solid';
import { debounce } from 'lodash';

interface SignInProps {
  signInData: object;
  setSignInData: Function;
}

const InputPassword: React.FC<SignInProps> = ({ signInData, setSignInData }) => {
  const [password, setPassword] = useState({ value: '', error: null });
  const isValid = !password.error;

  const handleInput = (e: any) => {
    const value = e.target.value;
    const validatePassword = yup.object().shape({
      value: yup.string().min(6).required(),
    });

    validatePassword.validate({ value })
      .then(() => {
        setPassword({ value, error: null });
      })
      .catch((error) => {
        const errors = error.errors;
        setPassword({ value, error: errors });
      });
  };

  useEffect(() => {
    setSignInData({
      ...signInData,
      password: { value: password.value, error: password.error },
    });
  }, [password]);

  return (
    <div>
      <label
        htmlFor='email'
        className='block text-sm font-medium text-gray-700 dark:text-gray-300'
      >
        Password
      </label>
      <div className='relative mt-1 rounded-md shadow-sm'>
        <input
          type='password'
          name='password'
          id='password'
          className='block w-full rounded-md border-indigo-600 pr-10 text-red-900 placeholder-red-300 focus:border-slate-500 focus:outline-none focus:ring-indigo-600 sm:text-sm'
          placeholder=''
          defaultValue=''
          aria-invalid='true'
          aria-describedby='email-error'
          onInput={debounce(handleInput, 300)}
        />
        {!isValid && (
          <div className='pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3'>
            <ExclamationCircleIcon
              className='h-5 w-5 text-red-500'
              aria-hidden='true'
            />
          </div>
        )}
      </div>
      {!isValid && (
        <p className='mt-2 text-sm text-red-600' id='email-error'>
          Password must contain at least 6 characters
        </p>
      )}
    </div>
  );
};

export default InputPassword;
