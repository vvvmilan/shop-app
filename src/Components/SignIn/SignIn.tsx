import React, { useEffect, useState } from 'react';
import InputEmail from './InputEmail';
import InputPassword from './InputPassword';
import Toggle from '../Toggle';
import logo from '../../img/logo.png';
import { useNavigate } from 'react-router-dom';

const SignIn = () => {
  const navigate = useNavigate();

  const [signInData, setSignInData] = useState({
      email: { value: '', error: null },
      password: { value: '', error: null },
    },
  );

  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    const isFormValid = Boolean(signInData.email.value)
      && Boolean(signInData.password.value)
      && !Object
        .values(signInData)
        .map(item => item.error)
        .some(error => error);
    setIsValid(isFormValid);
  }, [signInData]);

  const handleLogin = () => {
    const token = Math.random().toString(36).substr(2);
    localStorage.setItem('auth_token', token);
    navigate('/products');
  };

  return (
    <div className='flex min-h-fit flex-col justify-center py-40 sm:px-6 lg:px-8'>
      <div className='sm:mx-auto sm:w-full sm:max-w-md'>
        <h2 className='mt-6 text-center text-3xl font-bold tracking-tight text-gray-700 dark:text-gray-300'>
          Sign in to your account
        </h2>
      </div>

      <div className='mt-8 sm:mx-auto sm:w-full sm:max-w-md'>
        <div className='bg-gray-50 py-8 px-4 shadow sm:rounded-lg sm:px-10 dark:bg-slate-700'>
          <form className='space-y-6' action='src/Components#' method='POST'>
            <div>
              <div>
                <InputEmail
                  signInData={signInData}
                  setSignInData={setSignInData}
                />
              </div>
            </div>

            <div>
              <InputPassword
                signInData={signInData}
                setSignInData={setSignInData}
              />
            </div>

            <div>
              <button
                disabled={!isValid}
                type='button'
                className={`${isValid ? 'bg-indigo-600 hover:bg-indigo-700 focus:ring-offset-2' : 'bg-indigo-400' } mt-8 flex w-full justify-center rounded-md border border-transparent py-2 px-4 text-sm font-medium text-white shadow-sm`}
                onClick={handleLogin}
              >
                Sign in
              </button>
            </div>
          </form>
        </div>
      </div>

      <div className='mt-8 mx-auto'>
        {/*<Toggle />*/}
      </div>
    </div>
  );
};

export default SignIn;
