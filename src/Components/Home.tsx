import React from 'react';
import logo from '../img/logo.png';

function Home() {
  return (
    <div className='flex min-h-fit py-60 items-center flex-col justify-center sm:px-6 lg:px-8'>
      <div className='sm:mx-auto sm:w-full sm:max-w-md'>
        <img
          className='mx-auto h-36 w-auto'
          src={logo}
          alt='web shop logo'
        />
        <h2 className='mt-6 text-center text-3xl font-bold tracking-tight text-gray-700 dark:text-gray-300'>
          Welcome to web shop
        </h2>
      </div>
    </div>
  );
}

export default Home;