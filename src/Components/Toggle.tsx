import {useContext, useEffect, useState} from 'react';
import {PreferencesContext} from '../contexts/PreferencesProvider';
import {Switch} from '@headlessui/react';

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ');
}

const Toggle = () => {
  const {isThemeDark, handleThemeToggle} = useContext(PreferencesContext);

  return (
    <div className='mx-auto flex'>
      <div>
        <p className='mr-2 text-gray-700 dark:text-gray-300'>select theme</p>
      </div>
      <div>
        <Switch
          checked={isThemeDark}
          onClick={handleThemeToggle}
          className={classNames(
            isThemeDark ? 'bg-indigo-700' : 'bg-gray-400',
            'relative inline-flex h-6 w-11 flex-shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out'
          )}
        >
          <span className="sr-only">Use setting</span>
          <span
            className={classNames(
              isThemeDark ? 'translate-x-5' : 'translate-x-0',
              'pointer-events-none relative inline-block h-5 w-5 transform rounded-full bg-white shadow ring-0 transition duration-200 ease-in-out'
            )}
          >
            {/*toggle off*/}
            <span
              className={classNames(
                isThemeDark
                  ? 'opacity-0 ease-out duration-100'
                  : 'opacity-100 ease-in duration-200',
                'absolute inset-0 flex h-full w-full items-center justify-center transition-opacity'
              )}
              aria-hidden="true"
            >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="currentColor"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="text-indigo-600 w-4 h-4">
                  <path
                    d="M12 3v2.25m6.364.386l-1.591 1.591M21 12h-2.25m-.386 6.364l-1.591-1.591M12 18.75V21m-4.773-4.227l-1.591 1.591M5.25 12H3m4.227-4.773L5.636 5.636M15.75 12a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0z"
                  />
            </svg>
          </span>

            {/*toggle on*/}
            <span
              className={classNames(
                isThemeDark
                  ? 'opacity-100 ease-in duration-200 bg-slate-100 rounded-full'
                  : 'opacity-0 ease-out duration-100',
                'absolute inset-0 flex h-full w-full items-center justify-center transition-opacity'
              )}
              aria-hidden="true"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="text-indigo-600 dark:text-indigo-800 w-4 h-4">
                  <path
                    d="M21.752 15.002A9.718 9.718 0 0118 15.75c-5.385 0-9.75-4.365-9.75-9.75 0-1.33.266-2.597.748-3.752A9.753 9.753 0 003 11.25C3 16.635 7.365 21 12.75 21a9.753 9.753 0 009.002-5.998z"
                  />
              </svg>
            </span>
          </span>
        </Switch>
      </div>
    </div>
  );
};

export default Toggle;
