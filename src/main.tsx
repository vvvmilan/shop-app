import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import PreferencesProvider from "./contexts/PreferencesProvider";

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <PreferencesProvider>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </PreferencesProvider>
)
