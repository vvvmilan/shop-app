// import '/dist/output.css';
import './index.css'
import React from "react";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import Home from './Components/Home';
import Navbar from './Components/Navbar';
import PrivateRoutes from "./utils/PrivateRoutes";
import SingleProduct from './Components/SingleProduct';
import Products from "./Components/Products";
import SignIn from './Components/SignIn/SignIn';

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route element={<Home />} path='/' />
        <Route element={<SignIn />} path='/signin' />
        <Route element={<PrivateRoutes />}>
          <Route element={<Products />} path='/products'/>
          <Route element={<SingleProduct />} path='/product'/>
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
